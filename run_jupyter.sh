#!/usr/bin/env bash

readonly MAXWELL_PYTHON_VIRTUAL_ENV_DIR=~/bin/maxwell-python-virtual-env

source "$MAXWELL_PYTHON_VIRTUAL_ENV_DIR"/bin/activate
jupyter-lab "$@"
