#!/usr/bin/env bash

PYTHON_VERSION=3.10.6

MACOS=

die() {
  echo
  echo ❌ "$1"
  echo
  exit 1
}

info() {
  local msg="$1"
  echo "🔵 INFO - $msg"
}

success() {
  local msg="$1"
  echo
  echo "✅ OK - $msg"
  echo
}

check_brew() {
  if [ "$MACOS" == "true" ]
  then
    brew help > /dev/null || die "command 'brew help' failed, check your brew installation"
  fi
}

check_pyenv_deps_on_linux() {
  info "Install pyenv system dependencies, with sudo (may ask your password)"
  sudo apt-get install build-essential git libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev xz-utils tk-dev || die "Failed to install pyenv dependencies"
}

check_xcode_cli_tools() {
  xcode-select -p > /dev/null || die "Xcode command line tools not installed, please install it with command 'xcode-select --install'"
}

do_install_pyenv() {
  if [ "$MACOS" == "true" ]
  then
    check_xcode_cli_tools
    info "pyenv not installed, we will install it with 'brew install pyenv'"
    brew install pyenv
    return "$?"
  else
    check_pyenv_deps_on_linux
    info "pyenv not installed, we will install it with 'curl https://pyenv.run | bash'"
    rm -rf ~/.pyenv
    if ! curl https://pyenv.run | bash
    then
      return 1
    fi
    export PYENV_ROOT="$HOME/.pyenv"
    export PATH="$PYENV_ROOT/bin:$PATH"
    eval "$(pyenv init -)"
    return 0
  fi
}

install_pyenv() {
  if do_install_pyenv
  then
    success "pyenv installed successfully"
  else
    die "failed to install pyenv"
  fi
}

install_python() {
  info "install python $PYTHON_VERSION"
  if ! pyenv install "$PYTHON_VERSION"
  then
    die "failed to install python version $PYTHON_VERSION"
  fi
}

check_pyenv() {
  if ! pyenv --version > /dev/null 2>&1
  then
    install_pyenv
  else
    info "pyenv already installed: $(pyenv --version)"
  fi

  if pyenv versions|grep "$PYTHON_VERSION" > /dev/null
  then
    info "python $PYTHON_VERSION already installed"
  else
    install_python
  fi
  info "select version $PYTHON_VERSION by default"
  if ! pyenv global "$PYTHON_VERSION" > /dev/null
  then
    die "Failed to select version $PYTHON_VERSION"
  fi
}

check_macos() {
  if [ "$(uname| tr '[:upper:]' '[:lower:]')" == "darwin" ]
  then
    MACOS="true"
    info "running on macos, poor JFV 😛"
  else
    info "running on linux, poor Jojo 😛"
  fi
}

display_shell_instructions() {
  info "add the following to your shell startup script (.zshrc or .bashrc)"
  echo "################# PYENV #################"
  echo 'export PYENV_ROOT="$HOME"/.pyenv'
  echo 'export PIPENV_PYTHON="$PYENV_ROOT"/shims/python'
  echo 'export PATH="$PYENV_ROOT/bin:$PATH"'

  echo 'plugin=('
  echo  ' pyenv'
  echo ')'

  echo 'eval "$(pyenv init -)"'
  echo "#########################################"
}

check_macos
check_brew
check_pyenv
display_shell_instructions
