FROM debian:stable-slim

ENV DEBIAN_FRONTEND noninteractive

WORKDIR /

COPY install-maxwell-python-virtual-env.sh .
COPY install-maxwell-python-ia-libs.sh .
COPY conf/.jupyter /root/.jupyter
COPY run_jupyter.sh /

# hadolint ignore=DL3008
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    python3-pip \
    python3-venv \
    python-is-python3 \
    python3-dev \
    gcc \
	libopencv-dev \
	python3-opencv \
    libhdf5-dev && \
    \
    ./install-maxwell-python-virtual-env.sh && ./install-maxwell-python-ia-libs.sh && \
    \
    apt-get purge -y gcc python3-dev libopencv-dev libhdf5-dev && apt-get autoremove -y && rm -rf /var/lib/apt/lists/*

# Jupyter and Tensorboard ports
EXPOSE 8888 6006

VOLUME /code

CMD ["/run_jupyter.sh"]
