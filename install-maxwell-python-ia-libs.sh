#!/usr/bin/env bash

readonly MAXWELL_PYTHON_VIRTUAL_ENV_DIR=~/bin/maxwell-python-virtual-env

readonly REQUIREMENTS_FILE=/tmp/requirements.txt
readonly REQUIREMENTS_TENSORFLOW_FILE=/tmp/requirements-tensorflow.txt
readonly REQUIREMENTS_TENSORFLOW_MACOS_FILE=/tmp/requirements-tensorflow-macos.txt

extract_requirements() {
cat << EOF > "$REQUIREMENTS_FILE"
pytest
h5py==3.9.0                 ## a Pythonic interface to the HDF5 binary data format. It lets you store huge amounts of numerical data, and easily manipulate that data
imageio==2.31.1             ## a Python library that provides an easy interface to read and write a wide range of image data
Keras-Preprocessing==1.1.2
matplotlib==3.6.2           ## library for data visualizations
numpy==1.23.5               ## intended to manipulate matrices or multidimensional arrays as well as mathematical functions operating on these arrays
opencv-python==4.5.5.64     ## to read images
pandas==2.0.3               ## for data handling and analysis
scikit-image==0.21.0        ## to read images (ocr images)
scikit-learn==1.3.0         ## to have confusion matrix
seaborn==0.12.1             ## to plot some graphics
tqdm==4.64.1                ## to see the data loading
scipy==1.9.3                ##
jupyterlab==3.5.0           ## notebook IDE
EOF
}

extract_requirements_tensorflow() {
cat << EOF > "$REQUIREMENTS_TENSORFLOW_FILE"
tensorflow==2.13.0          ## used to create some rnn models
tensorboard==2.13.0         ## suite of web applications for inspecting and understanding TensorFlow runs and graphs
keras==2.13.1               ## library used to create the models
EOF
}

# TODO: upgrade
# https://github.com/tensorflow/tensorflow/issues/61382
extract_requirements_tensorflow_macos() {
cat << EOF > "$REQUIREMENTS_TENSORFLOW_MACOS_FILE"
tensorflow-macos==2.12.0
tensorboard==2.12.0          ## suite of web applications for inspecting and understanding TensorFlow runs and graphs
tensorflow-metal==0.8.0
keras==2.12.0                ## library used to create the models
EOF
}

cleanup() {
  rm -f "$REQUIREMENTS_FILE" "$REQUIREMENTS_TENSORFLOW_FILE" "$REQUIREMENTS_TENSORFLOW_MACOS_FILE"
}

die() {
  echo
  echo ❌ "$1"
  echo
  exit 1
}

info() {
  local msg="$1"
  echo "🔵 INFO - $msg"
}

success() {
  local msg="$1"
  echo
  echo "✅ OK - $msg"
  echo
}

check_python() {
  python --version > /dev/null || die "command 'python --version' failed, check your python installation"
  pip -V > /dev/null || die "command 'pip list' failed, check your python installation"

  info "$(python --version)"
  info "$(pip -V)"
}

activate_maxwell_python_virtual_env() {
  if ! source "$MAXWELL_PYTHON_VIRTUAL_ENV_DIR"/bin/activate
  then
    die "Failed to activate maxwell python virtual env: $MAXWELL_PYTHON_VIRTUAL_ENV_DIR"
  else
    info "maxwell python virtual env activated ($MAXWELL_PYTHON_VIRTUAL_ENV_DIR)"
  fi
}

MACOS_FLAVOUR="-macos"
tensorflow_flavour() {
  if [ "$(uname| tr '[:upper:]' '[:lower:]')" == "darwin" ]
  then
    echo "$MACOS_FLAVOUR"
  fi
}

activate_maxwell_python_virtual_env
check_python
extract_requirements
extract_requirements_tensorflow
extract_requirements_tensorflow_macos

if [ "$1" == "uninstall" ]
then
  pip uninstall -r "$REQUIREMENTS_FILE" -r "$REQUIREMENTS_TENSORFLOW_FILE" -r "$REQUIREMENTS_TENSORFLOW_MACOS_FILE" -y && cleanup
  exit 0
fi

if [ "$1" == "default" ]
then
  TENSORFLOW_FLAVOUR=
else
  TENSORFLOW_FLAVOUR=$(tensorflow_flavour)
fi

pip install -r "$REQUIREMENTS_FILE" -r /tmp/requirements-tensorflow"$TENSORFLOW_FLAVOUR".txt && \
python -c 'from tensorflow.python.client import device_lib ; print("\n\n🔵 Devices usable by tensorflow:\n\n%s" % device_lib.list_local_devices())' && \
cleanup
