#!/usr/bin/env bash

readonly MAXWELL_PYTHON_VIRTUAL_ENV_DIR=~/bin/maxwell-python-virtual-env

MACOS=

die() {
  echo
  echo ❌ "$1"
  echo
  exit 1
}

info() {
  local msg="$1"
  echo "🔵 INFO - $msg"
}

success() {
  local msg="$1"
  echo
  echo "✅ OK - $msg"
  echo
}

check_xcode_cli_tools() {
  xcode-select -p >/dev/null || die "Xcode command line tools not installed, please install it with command 'xcode-select --install'"
}

install_pyenv() {
  if do_install_pyenv; then
    success "pyenv installed successfully"
  else
    die "failed to install pyenv"
  fi
}

check_maxwell_python_virtual_env() {
  if [ -d "$MAXWELL_PYTHON_VIRTUAL_ENV_DIR" ]; then
    info "maxwell python virtual env already exists: $MAXWELL_PYTHON_VIRTUAL_ENV_DIR"
  else
    if ! /usr/bin/python3 -m venv "$MAXWELL_PYTHON_VIRTUAL_ENV_DIR"
    then
      die "Failed to create maxwell python virtual env: $MAXWELL_PYTHON_VIRTUAL_ENV_DIR"
    else
      info "maxwell python virtual env created ($MAXWELL_PYTHON_VIRTUAL_ENV_DIR)"
    fi
  fi
}

check_macos() {
  if [ "$(uname | tr '[:upper:]' '[:lower:]')" == "darwin" ]; then
    MACOS="true"
    info "running on macos, poor JFV 😛"
    check_xcode_cli_tools
  else
    info "running on linux, poor Jojo 😛"
  fi
}

check_macos
check_maxwell_python_virtual_env
